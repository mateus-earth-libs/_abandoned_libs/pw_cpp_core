//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Log.hpp                                                       //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <stdlib.h>
// PixWiz - Core
#include "CoreConfig.hpp"
#include "IO/Print.hpp"
#include "IO/Terminal.hpp"
#include "Debug/Debugger.hpp"

namespace pw { namespace Log {

//------------------------------------------------------------------------------
template <typename ...Args>
inline void
D(char const * const format, Args const & ... args)
{
    pw::Print(TermColor::Green("[DEBUG] "));
    pw::Println(format, args...);
}

template <typename ...Args>
inline void
I(char const * const format, Args const & ... args)
{
    pw::Print(TermColor::Blue("[INFO] "));
    pw::Println(format, args...);
    fflush(stdout);
}

template <typename ...Args>
inline void
W(char const * const format, Args const & ... args)
{
    pw::Print(TermColor::Yellow("[WARNING] "));
    pw::Println(format, args...);
}

template <typename ...Args>
inline void
E(char const * const format, Args const & ... args)
{
    pw::Print(TermColor::Red("[ERROR] "));
    pw::Println(format, args...);
}

template <typename TFormat, typename ...Args>
inline void
F(TFormat format, Args const & ... args)
{
    pw::Print(TermColor::Red("[FATAL] "));
    pw::Println(format, args...);

    #if PW_LOG_FATAL_SHOULD_DEBUG_BREAK
        PW_DEBUGGER_BREAK();
    #endif // PW_LOG_FATAL_SHOULD_DEBUG_BREAK

    exit(1);
}

} // namespace Log
} // namespace pw
