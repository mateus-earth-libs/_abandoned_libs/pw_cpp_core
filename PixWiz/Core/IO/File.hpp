//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : File.hpp                                                      //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <string>
#include <vector>
// PixWiz - Core
#include "Assert/Assert.hpp"
#include "Base/Base.hpp"
#include "Memory/Memory.hpp"

namespace pw { namespace File {

//----------------------------------------------------------------------------//
// Types                                                                      //
//----------------------------------------------------------------------------//
enum class OpenMode {
    READ,
    WRITE,
    APPEND,
    READ_WRITE,
    READ_APPEND
}; // class OpenMode

struct Handle_t;


//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
bool Exists(char const * const path);


Handle_t* Open    (char const * const path, OpenMode mode);
void      Close   (Handle_t *&handle);
size_t    GetSize (Handle_t *handle);
bool      Read    (Handle_t *handle, void *buffer, size_t size);
bool      Write   (Handle_t *handle, void const * const buffer, size_t size);

template<typename Type>
pw::Memory::MemBuffer<Type, size_t>
ReadAll(char const * const filename)
{
    using MemBuf_t = pw::Memory::MemBuffer<Type, size_t>;
    auto file_handle = Open(filename, OpenMode::READ);
    if(!file_handle) {
        return MemBuf_t::Null();
    }

    auto file_size = GetSize(file_handle);
    auto buffer    = MemBuf_t::Create(file_size / sizeof(Type));
    auto read_ok   = Read(file_handle, buffer.data, file_size);

    Close(file_handle);
    if(read_ok) {
        return buffer;
    }

    buffer.Destroy();
    return buffer;
}

inline pw::Memory::MemBuffer<char, size_t>
ReadAllText(char const * const filename)
{
    using MemBuf_t = pw::Memory::MemBuffer<char, size_t>;
    auto file_handle = Open(filename, OpenMode::READ);
    if(!file_handle) {
        return MemBuf_t::Null();
    }

    auto file_size = GetSize(file_handle);
    auto buffer    = MemBuf_t::Create(file_size / sizeof(u8) + 1);
    auto read_ok   = Read(file_handle, buffer.data, file_size);

    Close(file_handle);

    if(read_ok) {
        buffer.data[file_size] = '\0';
        return buffer;
    }

    buffer.Destroy();
    return buffer;
}


// constexpr auto FILE_LINES_HINT_DONT_CARE = pw::Max<u32>();

// struct FileLines_t {
//     void *_data;
//     u32 *lines;
//     u32 *lines_count;
// }; // struct FileLines_t

// Result<FileLines_t>
// ReadAllLines(
//     char const * const filename,
//     u32                lines_count_hint = FILE_LINES_HINT_DONT_CARE);

} // namespace File
} // namespace pw
