//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Print.hpp                                                     //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <stdio.h>
// pw - Detail
#include "Print_Detail.hpp"

namespace pw {

//----------------------------------------------------------------------------//
// printf-like Functions                                                      //
//----------------------------------------------------------------------------//
///-----------------------------------------------------------------------------
/// @brief XXX(stdmatt): Add comments...
template <typename ... Args>
inline void
Print(std::string const &format, Args const & ... args)
{
    pw::Detail::_Print(format, args...);
}

///-----------------------------------------------------------------------------
/// @brief XXX(stdmatt): Add comments...
template <typename ... Args>
inline void
Println(std::string const &format, Args const & ... args)
{
    pw::Print(format, args...);
    ::printf("\n");
}

} // namespace pw
