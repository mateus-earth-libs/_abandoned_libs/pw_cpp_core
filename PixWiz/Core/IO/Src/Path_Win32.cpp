﻿#include "Base/Discovery/OS.hpp"
#if PW_OS_WINDOWS
// Header
#include "IO/Path.hpp"
// Windows
#include <direct.h>
#include <Shlobj.h>
#include <Shlwapi.h>
#include <Lmcons.h> // For UNLEN (GetUserName)
#pragma comment(lib, "Shlwapi.lib")
// PixWiz - Core
#include "Platform/Win32.hpp"
#include "Memory/Memory.hpp"
namespace pw {


//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
static std::string
_MakeAbs(std::string const &path)
{
    char buffer[MAX_PATH] = {};
    auto len = GetFullPathName(path.c_str(), MAX_PATH, buffer, nullptr);
    PW_ASSERT(len != 0, "Failed to GetFullPathName of: (%s)", path.c_str());

    return buffer;
}

//
//
//
//------------------------------------------------------------------------------
std::string const&
Path::GetSeparator()
{
    static const std::string s_sep = "/";
    return s_sep;
}

//------------------------------------------------------------------------------
std::string const&
Path::GetAltSeparator()
{
    static const std::string s_sep = "\\";
    return s_sep;
}

//
//
//
//------------------------------------------------------------------------------
bool
Path::IsDir(const std::string &path)
{
    if(path.empty()) {
        return false;
    }

    DWORD const file_attrib = GetFileAttributesA(path.c_str());
    if(file_attrib & INVALID_FILE_ATTRIBUTES) {
        return false;
    }

    return (file_attrib & FILE_ATTRIBUTE_DIRECTORY) != 0;
}

//------------------------------------------------------------------------------
bool
Path::IsFile(const std::string &path)
{
    if(path.empty()) {
        return false;
    }

    DWORD const file_attrib = GetFileAttributesA(path.c_str());
    auto  const valid       = (file_attrib & INVALID_FILE_ATTRIBUTES);
    if(!valid) {
        return false;
    }

    // @XXX: find really the documentation about it because seems very strange
    // to check if the path is a file this way....
    auto const is_file = (file_attrib & FILE_ATTRIBUTE_DIRECTORY);
    return is_file == 0;
}

//------------------------------------------------------------------------------
bool
Path::IsAbs(std::string const &path)
{
    if(path.empty()) {
        return false;
    }
    return PathIsRoot(path.c_str()) == TRUE;
}
//
//
//
//------------------------------------------------------------------------------
std::string
Path::CurrentDirectory()
{
    return _MakeAbs(".");
}


//------------------------------------------------------------------------------
void
Path::SetCurrentDirectory(std::string const &path)
{
    // @todo: implement...
}


} // namespace pw
#endif // PW_OS_IS_WINDOWS
