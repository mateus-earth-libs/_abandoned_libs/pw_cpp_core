#include "Base/Discovery/OS.hpp"
#if (PW_OS_WINDOWS)
// Header
#include "IO/File.hpp"
// std
#include <stdio.h>
// PixWiz - Core
#include "IO/Path.hpp"

namespace pw {

//------------------------------------------------------------------------------
bool
File::Exists(char const * const path)
{
    return Path::IsFile(path);
}


File::Handle_t*
File::Open(char const * const path, File::OpenMode mode)
{
    char const * const open_mode =
        (mode == OpenMode::READ        ) ? "r"  :
        (mode == OpenMode::WRITE       ) ? "w"  :
        (mode == OpenMode::APPEND      ) ? "a"  :
        (mode == OpenMode::READ_WRITE  ) ? "rw" :
        (mode == OpenMode::READ_APPEND ) ? "ra" : nullptr;

    return (Handle_t*)fopen(path, open_mode);
}

//------------------------------------------------------------------------------
void
File::Close(Handle_t *&handle)
{
    fclose((FILE *)handle);
    handle = nullptr;
}

//------------------------------------------------------------------------------
size_t
File::GetSize(Handle_t *handle)
{
    auto curr = ftell((FILE*)handle);
    fseek((FILE*)handle, 0, SEEK_END);
    auto end  = ftell((FILE*)handle);
    fseek((FILE*)handle, curr, SEEK_SET);

    return end;
}


//------------------------------------------------------------------------------
bool
File::Read(Handle_t *handle, void *buffer, size_t size)
{
    auto rs = fread(buffer, 1, size, (FILE*)handle);
    return rs == size;
}

//------------------------------------------------------------------------------
bool
File::Write(Handle_t *handle, void const * const buffer, size_t size)
{
    auto ws = fwrite(buffer, 1, size,  (FILE*)handle);
    return ws == size;
}

} // namespace pw
#endif // (PW_OS_WINDOWS)
