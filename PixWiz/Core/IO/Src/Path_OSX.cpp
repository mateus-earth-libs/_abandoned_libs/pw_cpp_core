﻿#include "Base/Discovery/OS.hpp"
#if PW_OS_OSX

// Header
#include "IO/Path.hpp"
// unix
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

namespace pw {

//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
static bool
_Stat(char const * const path, unsigned mask)
{
    struct stat sb = {0};
    if(stat(path, &sb) != 0) {
        return false;
    }

    return (sb.st_mode & S_IFMT) == mask;
}

//
//
//
//------------------------------------------------------------------------------
std::string const&
Path::GetSeparator()
{
    static const std::string s_sep = "/";
    return s_sep;
}

//------------------------------------------------------------------------------
std::string const&
Path::GetAltSeparator()
{
    // @incomplete: Backslashes on windows.
    static const std::string s_sep = "/";
    return s_sep;
}

//
//
//
//------------------------------------------------------------------------------
bool
Path::IsDir(const std::string &path)
{
    return _Stat(path.c_str(), S_IFDIR);
}

//------------------------------------------------------------------------------
bool
Path::IsFile(const std::string &path)
{
    return _Stat(path.c_str(), S_IFREG);
}

//------------------------------------------------------------------------------
bool
Path::IsAbs(std::string const &path)
{
    if(path.empty()) {
        return false;
    }
    return path[0] == Path::GetSeparator   ()[0]
        || path[0] == Path::GetAltSeparator()[0];
}

//
//
//
//------------------------------------------------------------------------------
std::string
Path::CurrentDirectory()
{
    // @buggy: Find a reliable way to get the max path size
    auto buffer  = pw::Memory::Alloc<char>(1024);
    auto path    = getcwd(buffer, 1024);
    auto ret_val = std::string(path);
    Memory::Free(path);

    return ret_val;
}

//------------------------------------------------------------------------------
void
Path::SetCurrentDirectory(std::string const &path)
{
    // @todo: implement...
}

} // namespace pw
#endif // PW_OS_IS_OSX
