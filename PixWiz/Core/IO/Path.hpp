//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Path.hpp                                                      //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// std
#include <array>
#include <string>
#include <vector>
// PixWiz - Core
#include "String/String.hpp"

namespace pw { namespace Path {

//
// Separator
//
std::string const& GetSeparator   ();
std::string const& GetAltSeparator();

//
//
//
bool IsDir (std::string const &path);
bool IsFile(std::string const &path);

//
//
//
bool        IsAbs    (std::string const &path);
std::string Abs      (std::string const &path);
std::string Normalize(std::string        path, bool force_forward_slashes = true);

//
//
//
std::string CurrentDirectory   ();
void        SetCurrentDirectory(std::string const &path);

//
// Path Components
//
std::string                Basename(std::string const &path);
std::string                Dirname (std::string const &path);
std::array<std::string, 2> Split   (std::string const &path);

template <typename ...Args>
std::string
Join(std::string const &head, Args ...args)
{
    return StringUtils::Join(Path::GetSeparator(), head, args...);
}

//
// Extension
//
std::string                GetExtension   (std::string const &path);
std::string                ChangeExtension(std::string const &path, std::string const &new_extension);
std::string                RemoveExtension(std::string const &path);
std::array<std::string, 2> SplitExtension (std::string const &path);

} // namespace Path
} // namespace pw
