//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : CPU.hpp                                                       //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

//----------------------------------------------------------------------------//
// 32bit / 64bit                                                              //
//----------------------------------------------------------------------------//
// XXX(stdmatt): Add other processor archs...
#if (_WIN64)  || (__x86_64__) || (__ppc64__)
    #define PW_CPU_IS_32BIT 0
    #define PW_CPU_IS_64BIT 1
#else
    #define PW_CPU_IS_32BIT 1
    #define PW_CPU_IS_64BIT 0
#endif


//----------------------------------------------------------------------------//
// Intel x86 family                                                           //
// Reference:                                                                 //
//   http://nadeausoftware.com/articles/2012/02/c_c_tip_how_detect_processor_type_using_compiler_predefined_macros
// XXX(stdmatt): Implement for other processors families.                 //
//----------------------------------------------------------------------------//
#if defined(i386) || defined(__i386) || defined(__i386__) || defined(_M_IX86) || defined(_X86_)
    #define PW_CPU_IS_INTELX86  1
    #define PW_CPU_IS_INTELIA64 0
    #define PW_CPU_IS_ARM       0
#endif // Intel x86

//----------------------------------------------------------------------------//
// AMD64 family                                                               //
// Reference:                                                                 //
//   http://nadeausoftware.com/articles/2012/02/c_c_tip_how_detect_processor_type_using_compiler_predefined_macros
// XXX(stdmatt): Implement for other processors families.                 //
//----------------------------------------------------------------------------//
#if __x86_64 || __x86_64__ || __amd64 || __amd64__ || _M_AMD64 || _M_X64
    #define PW_CPU_IS_INTELX86  1
    #define PW_CPU_IS_INTELIA64 0
    #define PW_CPU_IS_ARM       0
#endif // AMD64
