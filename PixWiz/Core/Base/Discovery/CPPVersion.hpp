//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : CPPVersion.hpp                                                //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2018 - 2020                                         //
//                                                                            //
//  Description :                                                             //
//    Set of macros to enable us to discover which version of the             //
//    C++ standard the compiler is compiling the code against.                //
//                                                                            //
//    Useful to enable standard version specific constructs.                  //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// XXX(stdmatt): On MSVC this values are wrong...

//------------------------------------------------------------------------------
// C++ 98
#if (__cplusplus == 199711L)
    #define PW_CPP_98 1

//------------------------------------------------------------------------------
// C++ 11
#elif (__cplusplus == 201103L)
    #define PW_CPP_11 1

//------------------------------------------------------------------------------
// C++ 14
#elif (__cplusplus == 201402L)
    #define PW_CPP_14 1

//------------------------------------------------------------------------------
// C++ 20
#elif (__cplusplus == 201703L)
    #define PW_CPP_17 1

//------------------------------------------------------------------------------
// Can't detect the standard version.
#else
    #define PW_CPP_UKNOWN 1

#endif // #if (__cplusplus == 199711L)
