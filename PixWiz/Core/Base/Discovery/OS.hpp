//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : OS.hpp                                                        //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//  Description :                                                             //
//    Set of macros to enable us to discover the target operating system      //
//    that we're building the code.                                           //
//                                                                            //
//    Useful to enable operating system specific constructs.                  //
//---------------------------------------------------------------------------~//

#pragma once

//------------------------------------------------------------------------------
// BSDs
#if defined(   BSD         ) || \
    defined( __FreeBSD__   ) || \
    defined( __NetBSD__    ) || \
    defined( __OpenBSD__   ) || \
    defined( __bsdi__      ) || \
    defined( __DragonFly__ )

    #define PW_OS_BSD 1
#else
    #define PW_OS_BSD 0
#endif // BSDs


//------------------------------------------------------------------------------
// Cygwin
#if defined( __CYGWIN__ )
    #define PW_OS_CYGWIN 1
#else
    #define PW_OS_CYGWIN 0
#endif // Cygwin


//------------------------------------------------------------------------------
// GNU/Linux
#if defined ( __gnu_linux__ ) ||  \
    defined ( __linux__     )

    #define PW_OS_GNU_LINUX 1
#else
    #define PW_OS_GNU_LINUX  0
#endif // GNU/Linux


//------------------------------------------------------------------------------
// OSX
#if defined( __APPLE__ ) && \
    defined( __MACH__  )

    #define PW_OS_OSX 1
#else
    #define PW_OS_OSX 0
#endif // OSX


//------------------------------------------------------------------------------
// Windows
#if defined( _WIN32 )

    #define PW_OS_WINDOWS 1
#else
    #define PW_OS_WINDOWS 0
#endif // Windows


//------------------------------------------------------------------------------
// UNIX
#if PW_OS_BSD       || \
    PW_OS_CYGWIN    || \
    PW_OS_GNU_LINUX || \
    PW_OS_OSX

    #define PW_OS_UNIX 1
#else
    #define PW_OS_UNIX 0
#endif



#if PW_OS_BSD       || \
    PW_OS_CYGWIN    || \
    PW_OS_GNU_LINUX || \
    PW_OS_OSX       || \
    PW_OS_WINDOWS   || \
    PW_OS_UNIX
#else
    #error "OS NOT SUPPORTED";
#endif
