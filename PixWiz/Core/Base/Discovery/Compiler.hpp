//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Compiler.hpp                                                  //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2018 - 2020                                         //
//                                                                            //
//  Description :                                                             //
//    Set of macros to enable us to discover which compiler is building       //
//    the project.                                                            //
//                                                                            //
//    Useful to enable compiler specific constructs.                          //
//                                                                            //
//   Reference:                                                               //
//      https:sourceforge.net/p/predef/wiki/Compilers/                        //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

//----------------------------------------------------------------------------//
// Clang                                                                      //
//----------------------------------------------------------------------------//
#if defined(__clang__)
    #define PW_COMPILER_IS_CLANG 1
#else
    #define PW_COMPILER_IS_CLANG 0
#endif


//----------------------------------------------------------------------------//
// GCC                                                                        //
//----------------------------------------------------------------------------//
#if defined(__GNUC__)
    #define PW_COMPILER_IS_GCC 1
#else
    #define PW_COMPILER_IS_GCC 0
#endif


//----------------------------------------------------------------------------//
// Microsoft Visual C++                                                       //
//----------------------------------------------------------------------------//
#if defined(_MSC_VER)
    #define PW_COMPILER_IS_MSVC 1
#else
    #define PW_COMPILER_IS_MSVC 0
#endif
