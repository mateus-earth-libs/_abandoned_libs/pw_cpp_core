//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Base.hpp                                                      //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 19, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

#include "ClassUtils.hpp"
#include "NumericTypes.hpp"
#include "PointerUtils.hpp"
#include "MacroUtils.hpp"
#include "Result.hpp"

//
#include "Discovery/OS.hpp"
#include "Discovery/Compiler.hpp"
#include "Discovery/CPPVersion.hpp"
#include "Discovery/CPU.hpp"
