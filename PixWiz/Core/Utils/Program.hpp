//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Program.hpp                                                   //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once

// PixWiz - Core
#include "Base/Base.hpp"

namespace pw {

class Program
{
    //
    // Public Methods
    //
public:
    static void
    Set(u32 argc, char const *argv[])
    {
        Program::argc = argc;
        Program::argv = argv;
    }

    static u32
    GetArgc()
    {
        return argc;
    }

    static char const **
    GetArgv()
    {
        return argv;
    }

    //
    // iVars
    //
private:
    static u32          argc;
    static char const **argv;
}; // class Program


} // namespace pw
