//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Memory.hpp                                                    //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 12, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2018 - 2020                                         //
//                                                                            //
//  Description :                                                             //
//    Facilities to ease the memory handling...                               //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
// std
#include <stdlib.h>
// PixWiz - Core
#include "Base/Base.hpp"
#include "Assert/Assert.hpp"
// PixWiz - Memory
#include "Memory/Defer.hpp"

//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
namespace pw { namespace Memory {



///-----------------------------------------------------------------------------
/// @XXX
template <typename T>
inline T*
Alloc(size_t count)
{
    return (T*)malloc(sizeof(T) * count);
}

///-----------------------------------------------------------------------------
/// @XXX
template <typename T>
void
Free(T *&memory)
{
    ::free(memory);
    memory = nullptr;
}

template <typename T>
void
Delete(T *&object)
{
    PW_ASSERT_NOT_NULL(object);

    delete object;
    object = nullptr;
}



///-----------------------------------------------------------------------------
/// @brief
///   Zero the object's memory chunk. Same of calling memset(3) on a pointer
///   for that object, but with a nicer syntax and less error prone way.
/// @param t - Any object.
template <typename T>
inline void
Clear(T &t)
{
    pw_memset(&t, 0, sizeof(T));
}

///-----------------------------------------------------------------------------
/// @brief
///   Overload to prevent calling the function with a pointer instead
///   of a object.
// XXX(stdmatt)...
// template <typename T>
// inline void
// clear(T *&t)
// {
//     static_assert(false, "Value types only...");
// }

template <typename Type, typename SIZE_T = u32>
struct MemBuffer
{
    Type   *data   = nullptr;
    SIZE_T  count  = count;

    MemBuffer() = default;
    MemBuffer(Type *b, SIZE_T s): data(b), count(s)
    {
        PW_ASSERT(
            (b == nullptr && s == 0) || (b != nullptr && s != 0),
            "Creating a Memory Buffer if wrong buffer and count!"
        );
    }

    static MemBuffer Create(SIZE_T count) { return MemBuffer(Alloc<Type>(count), count); }
    static MemBuffer Null  ()             { return MemBuffer(nullptr,            0    ); }

    void
    Destroy()
    {
        PW_ASSERT_NOT_NULL(data);
        Free(data);
        count = 0;
    }

    bool
    IsValid()
    {
        return count != 0;
    }

    operator bool()
    {
        return IsValid();
    }

    SIZE_T
    SizeInBytes()
    {
        return count * sizeof(Type);
    }
}; // MemBuffer

} // namespace Memory
} // namespace pw
