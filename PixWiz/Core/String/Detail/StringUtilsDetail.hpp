//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : StringUtilsDetail.hpp                                         //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 18, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
#include <string>
#include <sstream>

//------------------------------------------------------------------------------
namespace pw {namespace StringUtils {

namespace Detail_Join {
    template <typename T>
    inline std::string
    Join(std::string const &separator, T const &value)
    {
        std::stringstream ss;
        ss << value << separator;
        return ss.str();
    }
    template <typename T>
    inline std::string
    Join(T const &value)
    {
        std::stringstream ss;
        ss << value;
        return ss.str();
    }
} // Detail_join


} // namespace StringUtils
} // namespace pw
