//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Assert.hpp                                                    //
//  Project   : PixWiz - Core                                                 //
//  Date      : Jul 16, 2020                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2020                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

#pragma once
#define PW_ASSERT(_expr_, fmt, ...)  do {} while(0)
#define PW_ASSERT_NULL(_expr_) do {} while(0)
#define PW_ASSERT_NOT_NULL(_expr_) do {} while(0)
#define PW_ASSERT_NOT_EMPTY(palettes_to_use) do {} while(0)
